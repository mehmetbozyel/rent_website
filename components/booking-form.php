<div class="container">
<div class="bhoechie-tab-content active col-xs-12">
    <div role="form" class="wpcf7" id="" lang="en-US">
        <div class="screen-reader-response"></div>
        <form id="request-form" action="" method="post" class="" >

            <div class="col-xs-12 form-fill-section">
                <div class=" col-sm-6 col-xs-12 pickupaddres">
                    <p>Pick Up Address*</p>
                    <p> <span class="your-pickup"><input type="text"
                                name="your-pickup" id="pick-address"
                                value="" size="40"
                                class="form-box"
                                aria-invalid="false" required/></span> </p>       
                </div>
                <div class=" col-sm-6 col-xs-12 pickupaddres">
                    <p>Drop Off Address*</p>
                    <p> <span class="your-drop"><input type="text"
                                name="your-drop" id="drop-address"
                                value="" size="40"
                                class="form-box"
                                aria-invalid="false" required/></span> </p>
                </div>
            </div>
            <div class="col-xs-12 form-fill-section">
                <div class=" col-sm-6 col-xs-12 pickupaddres">
                    <p>Number of Passengers*</p>
                    <p> <span class="passengers"><input type="number"
                                name="passengers" id="passengers"
                                value="" size="2"
                                min="1" max=""
                                class="form-box"
                                aria-invalid="false" required/></span> </p>
                </div>
                
                <div class=" col-sm-6 col-xs-12 pickupaddres">
                    <p>Date*</p>
                    <p> <span class="date">
                        <input class="form-box" type="datetime-local" id="meeting-time"
                            name="meeting-time" value=""
                            min="" max="" required/></span> </p>
                </div>
            </div>
            <div class="col-xs-12 form-fill-section">
                <div class=" col-sm-4 col-xs-12 pickupaddres">
                    <p>Name*</p>
                    <p> <span class="your-name"><input type="text"
                                name="your-name" id="your-name"
                                value="" size="40"
                                class="form-box"
                                aria-invalid="false" required/></span> </p>
                </div>
                <div class=" col-sm-4 col-xs-12 pickupaddres">
                    <p>Phone* (123-456-7891)</p>
                    <p> <span class="your-phone"><input type="tel"
                                name="your-phone" id="your-phone"
                                value="" size="40" placeholder="123-456-7891"
                                class="form-box"
                                aria-invalid="false"
                                pattern="[0-9]{3}-[0-9]{3}-[0-9]{4}" 
                                required/></span> </p>
                </div>
                <div class=" col-sm-4 col-xs-12 pickupaddres">
                    <p>Email*</p>
                    <p> <span class="your-email"><input type="email"
                                name="your-email" id="your-email"
                                value="" size="40"
                                class="form-box"
                                aria-invalid="false" required/></span> </p>
                </div>
            </div>
            <div class="col-md-12 form-fill-section">
                <div class=" col-sm-12 col-xs-12 pickupaddres">
                    <p>Comments</p>
                    <p> <span class="your-message"><textarea
                                name="your-message" id="your-message"
                                cols="40" rows="10"
                                class="form-textarea"
                                aria-invalid="false" ></textarea></span> </p>
                </div>
            </div>

            <div class="col-xs-12"> 
                <p><input name="submit" type="submit" value="GET QUOTE"
                    class="formbookingbtn center-block"/>
                </p>
            </div>

            <div class="wpcf7-response-output wpcf7-display-none"></div>
        </form>
    </div>
</div>
</div>

<script>
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth()+1; //January is 0!
    var yyyy = today.getFullYear();
    if(dd<10){
            dd='0'+dd
        } 
        if(mm<10){
            mm='0'+mm
        } 

    today = yyyy+'-'+mm+'-'+dd + 'T00:00';

    document.getElementById("meeting-time").min = today;
</script>

<script>
    $(document).on("submit", "#request-form", function(event){ //request-form id li form post edildiğinde
        event.preventDefault();
        var serialized = $(this).serialize();
        
            $.ajax({
                url: "https://stanstedcab.co.uk/mail-sender/mail.php", 
                type: "POST",             
                data: serialized,
                dataType: "json",
                statusCode: {
                    200: function(data, status) {
                            if (data.success) {
                                alert(data.message);
                                console.log('Success');
                            } else {
                                console.log('Unsuccess', data);
                                alert('Couldn\'t send email');
                            }
                        }
                },
            });
            document.getElementById("request-form").reset();
        
    });


</script>