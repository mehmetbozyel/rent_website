<div class="container">
    <div class="row">
        <div class="col-xs-12 our-fleet">
            <h4>OUR FLEET</h4>
            <h3>Choose From a Wide Range of Minicab Fleet</h3>
            <p>Choose from a wide selection of cars ranging from luxury saloon car to classic first-class MPVs,
                we have every type of car available to meet your needs. We also take custom orders and will help
                you acquire a specific minicab.</p>
            <div class="col-xs-12 chauffeur-carousel">
                <div id="Carousel" class="carousel slide">

                    <a data-slide="prev" href="#carousel"
                        class="left carousel-control testi-control testi-left" title="Previous"></a>
                    <a data-slide="next" href="#carousel"
                        class="right carousel-control testi-control testi-right" title="Next"></a>

                    <ol class="carousel-indicators indicators-custom">
                        <li data-target="#Carousel" data-slide-to="0" class="active"></li>
                        <li data-target="#Carousel" data-slide-to="1"></li>
                    </ol>
                    <div class="carousel-inner">
                        <div class='item active'>
                            <div class='rows'>
                                <div class="col-md-4 col-sm-4 col-xs-12 carousel-box">
                                    <a href="#booking-form" class="thumbnail-box"
                                        title="SALOON">
                                        <img data-lazy-src="./src/saloon-1.png"
                                            alt="SALOON" title="SALOON" class="thubnail-img">
                                        <div class="carousel-caption captionbtn">
                                            <button class="price-btn">SALOON</button>
                                        </div>
                                        <div class="fleet-name">

                                            <p>SALOON</p>
                                        </div>
                                        <div class="top-line"></div>
                                        <p><img data-lazy-src="./src/img/passenger.png"
                                                alt="4 Passengers" title="4 Passengers">
                                            4 Passengers</p>
                                        <p><img data-lazy-src="./src/img/luggage.png"
                                                alt="2 Check-in Bags" title="2 Check-in Bags">
                                            2 Check-in Bags</p>
                                        <p><img data-lazy-src="./src/img/bag.png"
                                                alt="2 Cabin Luggage" title="2 Cabin Luggage">
                                            2 Cabin Luggage</p>
                                        <p class="fleet-detail">
                                            If you are looking for a comfortable yet affordable private hire,
                                            then our saloon car is the perfect choice for you. </p>
                                    </a>
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-12 carousel-box">
                                    <a href="#booking-form" class="thumbnail-box"
                                        title="ESTATE">
                                        <img data-lazy-src="./src/estate-1.png"
                                            alt="ESTATE" class="thubnail-img" title="ESTATE">
                                        <div class="carousel-caption captionbtn">
                                            <button class="price-btn">ESTATE</button>
                                        </div>
                                        <div class="fleet-name">

                                            <p>ESTATE</p>
                                        </div>
                                        <div class="top-line"></div>
                                        <p><img data-lazy-src="./src/img/passenger.png"
                                                alt="4 Passengers" title="4 Passengers">
                                            4 Passengers</p>
                                        <p><img data-lazy-src="./src/img/luggage.png"
                                                alt="3 Check-in Bags" title="3 Check-in Bags"> 3
                                            Check-in Bags</p>
                                        <p><img data-lazy-src="./src/img/bag.png"
                                                alt="3 Cabin Luggage" title="3 Cabin Luggage"> 3 Cabin
                                            Luggage</p>
                                        <p class="fleet-detail">
                                            Whether you are looking for a cab for family weekends & short
                                            business trips, Our estate cars are the best option for you. </p>
                                    </a>
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-12 carousel-box">
                                    <a href="#booking-form" class="thumbnail-box"
                                        title="MPV 7">
                                        <img data-lazy-src="./src/mpv7-1.png"
                                            alt="MPV 7" class="thubnail-img" title="MPV 7">
                                        <div class="carousel-caption captionbtn">
                                            <button class="price-btn">MPV 7</button>
                                        </div>
                                        <div class="fleet-name">

                                            <p>MPV 7</p>
                                        </div>
                                        <div class="top-line"></div>
                                        <p><img data-lazy-src="./src/img/passenger.png"
                                                alt="6 Passengers" title="6 Passengers"> 6
                                            Passengers</p>
                                        <p><img data-lazy-src="./src/img/luggage.png"
                                                alt="2 Check-in Bags" title="2 Check-in Bags">
                                            2 Check-in Bags</p>
                                        <p><img data-lazy-src="./src/img/bag.png"
                                                alt="3 Cabin Luggage" title="3 Cabin Luggage"> 3 Cabin
                                            Luggage</p>
                                        <p class="fleet-detail">
                                            A1 Cars has the best MPV 7 cars in its fleet. Our MPV cars are ideal
                                            for group trips and are available at competitive rates. </p>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class='item'>
                            <div class='rows'>
                                <div class="col-md-4 col-sm-4 col-xs-12 carousel-box">
                                    <a href="#booking-form" class="thumbnail-box"
                                        title="Executive">
                                        <img data-lazy-src="./src/executive-1.png"
                                            alt="Executive" class="thubnail-img"
                                            title="Executive">
                                        <div class="carousel-caption captionbtn">
                                            <button class="price-btn">Executive</button>
                                        </div>
                                        <div class="fleet-name">

                                            <p>Executive</p>
                                        </div>
                                        <div class="top-line"></div>
                                        <p><img data-lazy-src="./src/img/passenger.png"
                                                alt="4 Passengers" title="4 Passengers">
                                            4 Passengers</p>
                                        <p><img data-lazy-src="./src/img/luggage.png"
                                                alt="2 Check-in Bags" title="2 Check-in Bags">
                                            2 Check-in Bags</p>
                                        <p><img data-lazy-src="./src/img/bag.png"
                                                alt="2 Cabin Luggage" title="2 Cabin Luggage">
                                            2 Cabin Luggage</p>
                                        <p class="fleet-detail">
                                            The Executive is the best suitable option for comfortable private or
                                            business travel with moderate amount of luggage. </p>
                                    </a>
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-12 carousel-box">
                                    <a href="#booking-form" class="thumbnail-box"
                                        title="Luxury">
                                        <img data-lazy-src="./src/luxury-1.png"
                                            alt="Luxury" class="thubnail-img" title="Luxury">
                                        <div class="carousel-caption captionbtn">
                                            <button class="price-btn">Luxury</button>
                                        </div>
                                        <div class="fleet-name">

                                            <p>Luxury</p>
                                        </div>
                                        <div class="top-line"></div>
                                        <p><img data-lazy-src="./src/img/passenger.png"
                                                alt="4 Passengers" title="4 Passengers">
                                            4 Passengers</p>
                                        <p><img data-lazy-src="./src/img/luggage.png"
                                                alt="2 Check-in Bags" title="2 Check-in Bags">
                                            2 Check-in Bags</p>
                                        <p><img data-lazy-src="./src/img/bag.png"
                                                alt="2 Cabin Luggage" title="2 Cabin Luggage">
                                            2 Cabin Luggage</p>
                                        <p class="fleet-detail">
                                            Our chauffeur driven Mercedes fleet is blend of luxury, sportiness &
                                            performance. These cars define class </p>
                                    </a>
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-12 carousel-box">
                                    <a href="#booking-form" class="thumbnail-box"
                                        title="MPV 8">
                                        <img data-lazy-src="./src/mpv8-1.png"
                                            alt="MPV 8" class="thubnail-img" title="MPV 8">
                                        <div class="carousel-caption captionbtn">
                                            <button class="price-btn">MPV 8</button>
                                        </div>
                                        <div class="fleet-name">

                                            <p>MPV 8</p>
                                        </div>
                                        <div class="top-line"></div>
                                        <p><img data-lazy-src="./src/img/passenger.png"
                                                alt="7 Passengers" title="7 Passengers"> 7
                                            Passengers</p>
                                        <p><img data-lazy-src="./src/img/luggage.png"
                                                alt="4 Check-in Bags" title="4 Check-in Bags"> 4
                                            Check-in Bags</p>
                                        <p><img data-lazy-src="./src/img/bag.png"
                                                alt="4 Cabin Luggage" title="4 Cabin Luggage"> 4
                                            Cabin Luggage</p>
                                        <p class="fleet-detail">
                                            These chauffeur driven cars are the best option for comfortable
                                            private or business travel with extra room for relaxing. </p>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>