<div class="container">
    <div class=" col-xs-12 footer">
        <div class="col-xs-12 col-sm-6 footer-about"> <img
                data-lazy-src="./src/stanstedcab-logo-sm.png"
                class="img-responsive" alt="Stansted Cab" title="Stansted Cab">
            <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium
             doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo 
             inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. 
             Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut</p>
        </div>

        <div class="col-xs-6 col-sm-3 license">
            <p><strong>Call Us</strong><br>
                <a href="tel:02088744000" title="Call Us On"> 020 8874 4000</p></a>
                
            <p><strong>Mail Us On</strong><br>
                <a href="mailto:info@stanstedcab.co.uk"
                title="Mail Us On"> info@stanstedcab.co.uk </p></a>
        </div>
    </div>
</div>