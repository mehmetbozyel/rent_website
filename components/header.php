<div class="container">
    <div class="row">
        <div class="col-xs-12 col-sm-2 col-md-4 logo"><a href="https://stanstedcab.co.uk/"
                title="Stansted Cab"><img
                    data-lazy-src="./src/stanstedcab-logo-sm.png"
                    class="img-responsive" alt="Stansted Cab" title="Stansted Cab"></a></div>
        <div class="col-xs-12 col-sm-10 col-md-8 booking">
            <div class="new-time">Fleet Working 24x7 - Call hours 8am-10pm. Outside call hours email or book
                online</div>

            <a href="tel:01111111111" class="header-tel" title="Call Us"
                onClick="ga('send', 'event', 'Phone Call Tracking', 'Click to Call', '011 1111 1111', 0);">011
                1111 1111</a>
            <a href="mailto:info@stanstedcab.co.uk"
                title="Mail Us On"> <span class="header-mail">
                    info@stanstedcab.co.uk</span></a>
            <a class="no-padding" href="#booking-form" title="BOOK ONLINE"><button
                    class="header-btn">BOOK ONLINE</button></a>
        </div>
    </div>
</div>