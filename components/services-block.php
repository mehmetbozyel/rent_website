<div class="container">
    <div class="col-xs-12 services">
        <h3>SERVICES</h3>
        <h5>Service is Our Attitude, and Attitude is Everything</h5>
        <div class="col-xs-12 services-content">
            <div class="col-xs-12 col-sm-6 services-text">
                We provide professional, reliable and quality services including corporate and private accounts.
                We pick up and drop off to all major
                <ul>
                    <li><img data-lazy-src="./src/img/bullets.png" />
                        Airports</li>
                    <li><img data-lazy-src="./src/img/bullets.png" />
                        Theatres</li>
                    <li><img data-lazy-src="./src/img/bullets.png" />
                        Hotels</li>
                    <li><img data-lazy-src="./src/img/bullets.png" />
                        Offices</li>
                    <li><img data-lazy-src="./src/img/bullets.png" />
                        Chauffeur Services</li>
                    <li><img data-lazy-src="./src/img/bullets.png" />
                        Light Removal</li>
                    <li><img data-lazy-src="./src/img/bullets.png" />
                        Regular Jobs</li>
                    <li><img data-lazy-src="./src/img/bullets.png" />
                        Residential Jobs</li>
        <!--        </ul> <a href="https://www.stanstedcab.co.uk" title="Read More"> <button
                        class="readmore-btn">Read More</button></a> -->
            </div>
            <div class="col-xs-12 col-sm-6 services-img">
                <img src="./src/services.jpg"
                    class="img-responsive" alt="services" title="services">
            </div>
        </div>
    </div>
</div>