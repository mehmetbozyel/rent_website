<div class="container">
    <div class="col-xs-12 testimonial">
        <h4>TESTIMONIALS</h4>
        <h6>We are Always Here to Serve You</h6>
        <div class="col-md-12" data-wow-delay="0.2s">
            <div class="carousel slide" data-ride="carousel" id="quote-carousel">


                <div class="carousel-inner text-center">
                    <div class="item active">
                        <blockquote>
                            <div class="row">
                                <div class="col-sm-12 testi-subtext">
                                    <h5>Lorem <br>Ipsum</h5>
                                    <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium
                                         doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo 
                                         inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. 
                                         Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit,
                                    </p>
                                </div>
                            </div>
                        </blockquote>
                    </div>
                    <div class="item ">
                        <blockquote>
                            <div class="row">
                                <div class="col-sm-12 testi-subtext">
                                    <h5>Lorem <br>Ipsum</h5>
                                    <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium
                                         doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo 
                                         inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. 
                                         Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit,
                                    </p>
                                </div>
                            </div>
                        </blockquote>
                    </div>
                    <div class="item ">
                        <blockquote>
                            <div class="row">
                                <div class="col-sm-12 testi-subtext">
                                    <h5>Lorem <br>Ipsum</h5>
                                    <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium
                                         doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo 
                                         inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. 
                                         Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit,</p>
                                </div>
                            </div>
                        </blockquote>
                    </div>
                </div>

                <a data-slide="prev" href="#quote-carousel"
                    class="left carousel-control testi-control testi-left" title="Previous"></a>
                <a data-slide="next" href="#quote-carousel"
                    class="right carousel-control testi-control testi-right" title="Next"></a>
            </div>
        </div>
    </div>
</div>