<div class="container">
    <div class="row">
        <div class="col-xs-12 welcome">
            <h2>Stansted Cab</h2>
            <h1>Leading Minicab Service Provider</h1>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore 
            magna aliqua.<br />
            Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
            </p>
            <div id="col-xs-12-io">
                <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
            </div>
            <div class="col-sm-4 col-xs-12 welcome-box">
                <div class="thumbnail-box">
                    <div class="rapid-img"></div>
                    <h3>Rapid City Transfer</h3>
                    <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                </div>
            </div>
            <div class="col-sm-4 col-xs-12 welcome-box">
                <div class="thumbnail-box">
                    <div class="baggaget-img"></div>
                    <h3>Baggage Transport</h3>
                    <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                </div>
            </div>
            <div class="col-sm-4 col-xs-12 welcome-box">
                <div class="thumbnail-box">
                    <div class="tourst-img"></div>
                    <h3> Tours</h3>
                    <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                </div>
            </div>
        </div>
    </div>
</div>