<html>
<head>
    
    <base href="https://stanstedcab.co.uk">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Minicab Services London | Airport Transfers - Heathrow &amp; Gatwick</title>
    <link rel="alternate" href="https://stanstedcab.co.uk/" hreflang="en-gb" />
    <link rel="shortcut icon" href="./src/img/fevi.png">

    <meta name="description"
        content="Put here summary your web page. Search engines typically show the Meta description in search results below your Title tag." />
    <link rel="canonical" href="https://stanstedcab.co.uk/" />

    <script
        type='application/ld+json'>{
            "@context":"https:\/\/schema.org",
            "@type":"WebSite",
            "@id":"#website",
            "url":"https:\/\/www.stanstedcab.co.uk\/",
            "name":"Stansted Cab",
            "potentialAction":{
                "@type":"SearchAction",
                "target":"https:\/\/www.stanstedcab.co.uk\/?s={search_term_string}",
                "query-input":"required name=search_term_string"
            }
        }
    </script>

    <!-- <link rel='dns-prefetch' href='//s.w.org' />  -->

    <link rel='stylesheet' id='bootstrap-css'
        href='./css/bootstrap.min.css'
        type='text/css' media='all' />
    <link rel='stylesheet' id='font-awesome.min-css'
        href='./css/font-awesome.min.css'
        type='text/css' media='all' />
    <link rel='stylesheet' id='style-css'
        href='./css/style.css' 
        type='text/css' media='all' />

    <script
        src="https://code.jquery.com/jquery-3.5.1.min.js"
        integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
        crossorigin="anonymous"></script>

    <script type='text/javascript'
        src='./js/bootstrap.min.js'></script>

    <script type="text/javascript"
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyANM48jn7HSEFqJtivN4j8wYRrKm4v_EfY&libraries=places"></script>

    <script type="text/javascript">
        function initialize() {
            var input = document.getElementById('pick-address');
            var inputs = document.getElementById('drop-address');
            var autocomplete = new google.maps.places.Autocomplete(input);
            var autocompletes = new google.maps.places.Autocomplete(inputs);
        }
        google.maps.event.addDomListener(window, 'load', initialize);
    </script>

    <script src="https://www.google.com/recaptcha/api.js" async defer></script>

</head>

<body>
    <div class="col-xs-12 header">
        <?php include './components/header.php';?>
    </div>

    <div class="carousel-inner">
        <div class="item active">
            <img data-lazy-src="./src/banner.jpg"
                class="banner-img" alt="Stansted Cab" title="Stansted Cab">
            <div class="carousel-caption caption-customiz">
                <h2 style="text-shadow: 3px 3px 4px #000;">Fast & Reliable</h2>
                <h4 style="text-shadow: 3px 3px 4px #000;">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</h4>
                <a href="#booking-form" title="BOOK NOW"><button
                        class="banner-btn">BOOK NOW</button></a>
                <div class="">
                <a href="#welcome-section">
                    <img data-lazy-src="./src/img/scroll-down2.gif"
                    class="arrow-size" alt="" title="" ></a>
                </div> 
            </div>
        </div>
    </div>

    <div id="welcome-section" class="col-xs-12 welcome-section">
        <?php include './components/welcome-section.php';?>
    </div>

    <div id="booking-form" class="form-block col-xs-12">
        <?php include './components/booking-form.php';?>
    </div>



    <div class="col-xs-12 call-block">
        <div class="container">
            <div class="col-xs-12 call-section">
                <h2>Call Us To Book a Cab</h2>
                <div class="h1"><a href="tel:02089471717">02089471717</a> - <a href="tel:08000326328">08000326328</a>
                </div>
                <a href="#booking-form" title="BOOK ONLINE"><button
                        class="banner-btn">BOOK ONLINE</button></a>
            </div>
        </div>
    </div>

    <div class="col-xs-12 services-block">
        <?php include './components/services-block.php';?>
    </div>

    <div class="col-xs-12 testimonial-block">
        <?php include './components/testimonial-block.php';?>
    </div>

    <div class="col-xs-12 footer-block">
        <?php include './components/footer.php';?>
    </div>

    <a href="#booking-form" title="BOOK NOW">
    <button class="banner-btn" id="myBtn">BOOK NOW</button></a>

    <script>
        (function (w, d) {
            var b = d.getElementsByTagName("body")[0];
            var s = d.createElement("script"); s.async = true;
            s.src = !("IntersectionObserver" in w) ? "./js/lazyload-8.17.min.js" : "./js/lazyload-10.19.min.js";
            w.lazyLoadOptions = {
                elements_selector: "img[data-lazy-src],.rocket-lazyload-bg",
                data_src: "lazy-src",
                data_srcset: "lazy-srcset",
                data_sizes: "lazy-sizes",
                skip_invisible: false,
                class_loading: "lazyloading",
                class_loaded: "lazyloaded",
                threshold: 300,
                callback_load: function (element) {
                    if (element.tagName === "IFRAME" && element.dataset.rocketLazyload == "fitvidscompatible") {
                        if (element.classList.contains("lazyloaded")) {
                            if (typeof window.jQuery != "undefined") {
                                if (jQuery.fn.fitVids) {
                                    jQuery(element).parent().fitVids();
                                }
                            }
                        }
                    }
                }
            }; // Your options here. See "recipes" for more information about async.
            b.appendChild(s);
        }(window, document));

        // Listen to the Initialized event
        window.addEventListener('LazyLoad::Initialized', function (e) {
            // Get the instance and puts it in the lazyLoadInstance variable
            var lazyLoadInstance = e.detail.instance;

            if (window.MutationObserver) {
                var observer = new MutationObserver(function (mutations) {
                    mutations.forEach(function (mutation) {
                        lazyLoadInstance.update();
                    });
                });

                var b = document.getElementsByTagName("body")[0];
                var config = { childList: true, subtree: true };

                observer.observe(b, config);
            }
        }, false);
    </script>

</body>
</html>