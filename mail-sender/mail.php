<?php
	$response = array();

	if (isset($_POST)){
		if(isset($_POST["your-pickup"]) && isset($_POST["your-drop"]) && isset($_POST["passengers"]) && 
				isset($_POST["meeting-time"]) && isset($_POST["your-name"]) && isset($_POST["your-phone"]) && 
				isset($_POST["your-email"]) ) {

			$pickup = htmlspecialchars(trim($_POST['your-pickup']));
			$drop = htmlspecialchars(trim($_POST['your-drop']));
			$passengers = htmlspecialchars(trim($_POST['passengers']));
			$meeting_time = htmlspecialchars(trim($_POST['meeting-time']));
			$name = htmlspecialchars(trim($_POST['your-name']));
			$phone = htmlspecialchars(trim($_POST['your-phone']));
			$email = htmlspecialchars(trim($_POST['your-email']));
			$message = htmlspecialchars(trim($_POST['your-message']));

			$time_arr = (explode("T",$meeting_time));
			$meeting_date = $time_arr[0];
			$meeting_time = $time_arr[1];

			$msg = "Pickup Place: $pickup <br />
					Drop Place: $drop <br />
					Number of Passengers: $passengers <br />
					Meeting Date: $meeting_date <br />
					Meeting Time: $meeting_time <br />
					Name: $name <br />
					Phone Number: $phone <br />
					Email: $email <br />
					Message: $message
				";

			$headers = "Content-Type: text/html; charset=UTF-8";

			$response["msg"] = $msg;

			mail("info@stanstedcab.co.uk", "New Car Request from ". $name,
			$msg, "From: mail@stanstedcab.co.uk" ."\r\n". $headers);

			$message = "Request sent for $name";
			$response["success"] = true;
			$response["message"] = $message;
			echo json_encode($response);

		}else{
			$response = array('result' => 'Fill all fields.', 'success' => false);
			echo json_encode($response);
			return $response;
		}
	}

	
	// print_r($_POST['your-pickup']);
?>